<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Auto;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TovarController extends AbstractController
{
    /**
     * @Route("/tovars/create", name="tovars_create", methods={"GET"})
     */
    public function create(EntityManagerInterface $em): Response
    {
        $tovar = new Product();
        $tovar
            ->setName('Ручка Berlingo')
            ->setPrice(34)
            ->setAmount(15);

        $em->persist($tovar);
        $em->flush();

        return new Response('Product created');
    }

    /**
     * @Route("/tovars/{id}", name="tovar_get_item", methods={"GET"})
     */
    public function readItem(EntityManagerInterface $em, int $id): Response
    {
        $tovar = $em->getRepository(Product::class)->find($id);

        return new Response($tovar ? $tovar->getName() : 'Product not found');
    }

    /**
     * @Route("/tovars", name="tovar_get_all", methods={"GET"})
     */
    public function readAll(EntityManagerInterface $em): Response
    {
        $tovars = $em->getRepository(Product::class)->findAll();
        dd($tovars);
        return new Response($tovars);
    }

    /**
     * @Route("/tovars/{id}/update", name="tovar_update", methods={"GET"})
     */
    public function updateItem(EntityManagerInterface $em, int $id): Response
    {
        $tovar = $em->getRepository(Product::class)->find($id);

        if ($tovar) {
            $tovar->setname('Ручка Favorite');

            $em->persist($tovar);
            $em->flush();

            return new Response('Product update');
        }

        return new Response('Product not found');
    }

    /**
     * @Route("/tovars/{id}/delete", name="tovar_delete", methods={"GET"})
     */
    public function deleteItem(EntityManagerInterface $em, int $id): Response
    {
        $tovar = $em->getRepository(Product::class)->find($id);

        if ($tovar) {
            $em->remove($tovar);
            $em->flush();

            return new Response('Product delete');
        }

        return new Response('Product not found');
    }
}