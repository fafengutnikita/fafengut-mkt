<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        $phrases = [
            'Hello', 'Hi', 'fdfdfs',
        ];

        return $this->render('test/test.html.twig', [
            'dateBr' => new \DateTime(),
            'phrases' => $phrases
        ]);
    }

    /**
     * @Route("/testf", name="testf_action")
     */
    public function  executeFction(): Response
    {
        return new Response('Hello');
    }
}