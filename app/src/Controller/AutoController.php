<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Auto;
use App\Entity\Human;
use App\Form\AutoType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AutoController extends AbstractController
{
    /**
     * @Route("/autos/create", name="auto_create", methods={"GET", "POST"})
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(AutoType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $auto = $form->getData();
            $em->persist($auto);
            $em->flush();

            return new Response($auto->getId());
        }

        return $this->renderForm('auto/create.html.twig', ['form' => $form]);
    }

    /**
     * @Route("/autos/{id}/add_human", name="auto_add_human", methods={"GET"})
     */
    public function addHumanToAuto(EntityManagerInterface $em, int $id): Response
    {
        $auto = $em->getRepository(Auto::class)->find($id);

        if ($auto === null) {
            return new Response('Auto not found');
        }

        $human = $em->getRepository(Human::class)->find(3);

        /* @var Auto $auto */
        $auto->setHuman($human);

        $em->persist($auto);
        $em->flush();

        return new Response('Human added to auto');
    }
}