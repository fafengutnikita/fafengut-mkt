<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PenController extends AbstractController
{
    /**
     * @Route("/pen", name="pen_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/pen.html.twig');
    }

}