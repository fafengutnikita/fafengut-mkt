<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NotebookController extends AbstractController
{
    /**
     * @Route("/notebook", name="notebook_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/notebook.html.twig');
    }

}