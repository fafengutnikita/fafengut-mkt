<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DiscountsController extends AbstractController
{
    /**
     * @Route("/discounts", name="discounts_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/discounts.html.twig');
    }

}