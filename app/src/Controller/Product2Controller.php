<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product2Controller extends AbstractController
{
    /**
     * @Route("/product2", name="product2_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product2.html.twig');
    }

}