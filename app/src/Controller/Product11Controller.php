<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product11Controller extends AbstractController
{
    /**
     * @Route("/product11", name="product11_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product11.html.twig');
    }

}