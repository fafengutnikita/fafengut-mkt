<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product10Controller extends AbstractController
{
    /**
     * @Route("/product10", name="product10_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product10.html.twig');
    }

}