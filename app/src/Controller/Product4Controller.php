<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product4Controller extends AbstractController
{
    /**
     * @Route("/product4", name="product4_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product4.html.twig');
    }

}