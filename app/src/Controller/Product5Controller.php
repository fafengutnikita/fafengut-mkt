<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product5Controller extends AbstractController
{
    /**
     * @Route("/product5", name="product5_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product5.html.twig');
    }

}