<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product7Controller extends AbstractController
{
    /**
     * @Route("/product7", name="product7_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product7.html.twig');
    }

}