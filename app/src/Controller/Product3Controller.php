<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product3Controller extends AbstractController
{
    /**
     * @Route("/product3", name="product3_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product3.html.twig');
    }

}