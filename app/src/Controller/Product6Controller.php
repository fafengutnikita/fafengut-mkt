<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product6Controller extends AbstractController
{
    /**
     * @Route("/product6", name="product6_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product6.html.twig');
    }

}