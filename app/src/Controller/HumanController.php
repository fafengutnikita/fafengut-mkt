<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Auto;
use App\Entity\Human;
use App\Form\HumanType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HumanController extends AbstractController
{
    /**
     * @Route("/humans/create", name="human_create", methods={"GET", "POST"})
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(HumanType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $human = $form->getData();
            $em->persist($human);
            $em->flush();

            return $this->redirectToRoute('human_get_item', ['id' => $human->getId()]);
        }

        return $this->renderForm('human/create.html.twig', ['form' => $form]);
    }

    /**
     * @Route("/humans/{id}", name="human_get_item", methods={"GET"})
     */
    public function readItem(EntityManagerInterface $em, int $id): Response
    {
        $human = $em->getRepository(Human::class)->find($id);

        return new Response($human ? $human->getName() : 'Human not found');
    }

    /**
     * @Route("/humans", name="human_get_list", methods={"GET"})
     */
    public function readList(EntityManagerInterface $em): Response
    {
        $humans = $em->getRepository(Human::class)->findAll();

        return new Response('Human list');
    }

    /**
     * @Route("/humans/{id}/update", name="human_update", methods={"GET"})
     */
    public function update(EntityManagerInterface $em, int $id): Response
    {
        $human = $em->getRepository(Human::class)->find($id);

        if ($human) {
            $human->setName('UpdateName');

            $em->persist($human);
            $em->flush();

            return new Response('Human updated');
        }

        return new Response('Human not found');
    }

    /**
     * @Route("/humans/{id}/delete", name="human_delete", methods={"GET"})
     */
    public function delete(EntityManagerInterface $em, int $id): Response
    {
        $human = $em->getRepository(Human::class)->find($id);

        if ($human) {
            $em->remove($human);
            $em->flush();

            return new Response('Human deleted');
        }

        return new Response('Human not found');
    }

    /**
     * @Route("/humans/{id}/autos", name="human_auto_list", methods={"GET"})
     */
    public function getAutoList(EntityManagerInterface $em, int $id): Response
    {
        $human = $em->getRepository(Human::class)->find($id);

        if ($human === null) {
            return new Response('Human not found');
        }

        /* @var Human $human */
        $autos = $human->getAutos();

        $autosTitle = [];
        foreach ($autos as $auto) {
            $autosTitle[] = $auto->getTitle();
        }

        return new Response('Human has ' . count($autos) . ' autos (' . join(',', $autosTitle) . ')');
    }

    /**
     * @Route("/humans/{id}/autos/{autoId}/add", name="human_add_auto", methods={"GET"})
     */
    public function addAuto(EntityManagerInterface $em, int $id, int $autoId): Response
    {
        $human = $em->getRepository(Human::class)->find($id);

        if ($human === null) {
            return new Response('Human not found');
        }

        $auto = $em->getRepository(Auto::class)->find($autoId);

        if ($auto === null) {
            return new Response('Auto not found');
        }

        /* @var Human $human */
        $human->addAuto($auto);

        $em->persist($human);
        $em->flush();

        return new Response('Added auto to human');
    }

    /**
     * @Route("/humans/{id}/autos/{autoId}/remove", name="human_remove_auto", methods={"GET"})
     */
    public function removeAuto(EntityManagerInterface $em, int $id, int $autoId): Response
    {
        $human = $em->getRepository(Human::class)->find($id);

        if ($human === null) {
            return new Response('Human not found');
        }

        $auto = $em->getRepository(Auto::class)->find($autoId);

        if ($auto === null) {
            return new Response('Auto not found');
        }

        /* @var Human $human */
        $human->removeAuto($auto);

        $em->persist($human);
        $em->flush();

        return new Response('Removed auto to human');
    }
}