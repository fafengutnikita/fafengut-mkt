<?php declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Product9Controller extends AbstractController
{
    /**
     * @Route("/product9", name="product9_action", methods={"GET"})
     */
    public function executeAction(): Response
    {
        return $this->render('site/product9.html.twig');
    }

}