<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Auto;
use App\Entity\Human;
use App\Repository\HumanRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AutoType extends AbstractType
{
    private HumanRepository $repository;

    /**
     * @param HumanRepository $repository
     */
    public function __construct(HumanRepository $repository)
    {
        $this->repository = $repository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $humans = $this->repository->findAll();

        $builder
            ->add('title', TextType::class, ['label' => 'Имя', 'required' => true])
            ->add('year', TextType::class, ['label' => 'Возраст'])
            ->add('human', ChoiceType::class, [
                'choices' => $humans,
                'choice_value' => function (?Human $entity) {
                    return $entity ? $entity->getId() : '';
                }
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Auto::class,
        ]);
    }
}