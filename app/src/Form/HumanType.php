<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Auto;
use App\Entity\Human;
use App\Repository\AutoRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HumanType extends AbstractType
{
    private AutoRepository $autoRepository;

    public function __construct(AutoRepository $autoRepository)
    {
        $this->autoRepository = $autoRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $autos = $this->autoRepository->getAutosWithoutHuman();

        $builder
            ->add('name', TextType::class, ['label' => 'Имя', 'required' => true])
            ->add('age', TextType::class, ['label' => 'Возраст'])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Human::class,
        ]);
    }
}