# Backend

## Creating your repository

- Copying the project from the repository `git clone path_to_repository new_project_name`
- Go to project directory `cd new_project_name`
- Remove .git directory `rm -rf .git`
- Git initialize `git init --initial-branch=main`
- Create your repository in GitLab
- Linking to the new repository `git remote add origin ...`

## Project deployment

- run `echo '127.0.0.1 project.local.ru' | sudo tee -a /etc/hosts` — it will allow you to visit `http://project.local.ru` in your browser
- `export DGID=$(id -g) && export DUID=$(id -u)` — these variables needs by Docker to build application container
- `docker-compose pull` — download all necessary Docker images
- `docker-compose build` — build local images
- `docker-compose up -d` — start containers.

As a result, you will have a bunch of containers with working application. Remember that **all commands must be run inside the `app` container**. In other words, you SHOULD NOT run Symfony console like `bin/console`, but only `docker-compose exec app bin/console`.

Next steps:

- place `.env.local` file with project variables to root project directory.
- install dependencies: `docker-compose exec app composer install`
- apply database migrations: `docker-compose exec app bin/console doctrine:migrations:migrate`

## Reset application to zero state

- `doctrine:database:drop --force` will delete the whole database;
- `doctrine:database:create` will create empty database;
- `doctrine:migrations:migrate` will apply migrations

You can apply fixtures with `doctrine:fixtures:load` command. Remember that this command will **DROP ALL EXISTING DATA** from your local database.